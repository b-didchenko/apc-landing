(function ($) {
    "use strict";

    // Internet Explorer 10 in Windows Phone 8 viewport bug fix
    if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
        var msViewportStyle = document.createElement('style');
        msViewportStyle.appendChild(
            document.createTextNode(
                '@-ms-viewport{width:auto!important}'
            )
        );
        document.head.appendChild(msViewportStyle);
    }

    $(document).ready(function () {

        var isMob = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

        if (!isMob) {

            $("#fullpage").fullpage({
                css3: true,
                navigation: true,
                navigationPosition: "right",
                slidesNavigation: true,
                responsiveHeight: 550,
                responsiveWidth: 991,
                touchSensitivity: 15,
                animateAnchor: false,
                anchors: ['slide1', 'slide2', 'slide3', 'slide4', 'slide5', 'slide6', 'slide7', 'slide8'],
                verticalCentered: false,
                hybrid: true,
                fitToSection: false,

                onLeave (index, nextIndex, direction) {
                    if ((nextIndex == 7) && (direction == "down")) $.fn.fullpage.fitToSection();
//                    if ((nextIndex == 5) && (direction == "up")) $.fn.fullpage.fitToSection();
                },

                afterResponsive () {
                    $.fn.fullpage.destroy('all');
                },


            });

            $('html').addClass('desktop');

        } else {

            document.querySelectorAll('.section').forEach((item, count) => {
                $(item).attr('id', `slide${count + 1}`)
            })

            $('.navigation a').on('click', (e) => {
                var target = $(e.target).attr('href');

                $('body, html').animate({
                    scrollTop: `${$(target).offset().top}px`
                }, '1000');

                return false;
            })
        }

        Site.navigationToggle();
    });

    var Site = new function () {

        this.navigationToggle = function () {
            var navBtn = $('.nav-trigger');

            navBtn.on('click', e => {

                navBtn.toggleClass('is-active');
                e.stopPropagation();

            });

            $(document).on('click', e => {
                if (!$(e.target).closest('.navigation').length) {
                    navBtn.removeClass('is-active')
                }
            })

        }
    } ();

})(jQuery);
